package com.example.demo.services;



import com.example.demo.entities.Todo;
import com.example.demo.repositories.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TodoServiceImplementation implements TodoService {

    @Autowired
    TodoRepository todoRepository;

    @Override
    public List<Todo> getAllTodos() {

        return new ArrayList<>(todoRepository.findAll());

    }

    @Override
    public Optional<Todo> getTodoById(String id) {
        return todoRepository.findById(id);
    }

    @Override
    public void createTodo(Todo todo) {
        Todo todoObject = todoRepository.save(new Todo(todo.getTitle(), todo.getDescription(), false));

    }

    @Override
    public void updateTodo(String id, Todo todo) {
        Optional<Todo> todoData = todoRepository.findById(id);
        if (todoData.isPresent()) {
            Todo todoObject = todoData.get();
            todoObject.setTitle(todo.getTitle());
            todoObject.setDescription(todo.getDescription());
            todoObject.setCompleted(todo.isCompleted());
            todoRepository.save(todoObject);

        }

    }

    @Override
    public void deleteTodo(String id) {
        todoRepository.deleteById(id);
    }

    @Override
    public void deleteAllTodos() {
        todoRepository.deleteAll();
    }

    @Override
    public List<Todo> getAllActiveTodos() {
        return todoRepository.findByCompleted(false);

    }
}

