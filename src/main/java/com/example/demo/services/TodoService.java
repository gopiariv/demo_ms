package com.example.demo.services;

import com.example.demo.entities.Todo;

import java.util.List;
import java.util.Optional;

public interface TodoService {
    List<Todo> getAllTodos();

    Optional<Todo> getTodoById(String id);

    void createTodo(Todo todo);

    void updateTodo(String id, Todo todo);

    void deleteTodo(String id);

    void deleteAllTodos();

    List<Todo> getAllActiveTodos();

}
