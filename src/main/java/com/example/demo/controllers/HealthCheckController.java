package com.example.demo.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {

    public static final String OK = "ok";

    @GetMapping("/healthcheck")
    @ResponseBody
    public String index() {
        return OK;
    }

}