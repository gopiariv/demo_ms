package com.example.demo.repositories;

import com.example.demo.entities.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TodoRepository extends MongoRepository<Todo, String> {
    List<Todo> findByTitleContaining(String title);

    List<Todo> findByCompleted(boolean completed);
}

